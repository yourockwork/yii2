<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 29.08.2019
 * Time: 10:08
 */

namespace app\controllers;

use Yii;
use app\models\Country;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Request;

class ActiveformController extends Controller
{
	public function actionIndex(){
		$model = Country::findOne(['code' => 'AU']);

		return $this->render('index', ['model' => $model]);
	}

	public function actionForm(){
		$data = Yii::$app->request->post();
		VarDumper::dump($data['Country']['code'], 10, true);die();
	}

	public function actionAjax(){
		$data = Yii::$app->request->post();
		$model = new Country(['code' => $data['Country']['code'], 'name' => $data['Country']['name'], 'population' => 300000000, 'date' => '2019-08-29']);
		$model->save();
//		VarDumper::dump(\Yii::$app->request->post(), 10, true);die();
		return 'good';
	}
}