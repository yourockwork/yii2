<?php

namespace app\controllers;


use app\models\Country;
use app\models\User;
use yii\web\Controller;

class HelpersController extends Controller
{
	public function actionIndex(){
		return $this->render('index');
	}

	public function actionForm(){
		$model = Country::findOne(['code' => 'AU']);
		//$models = Country::deleteAll();

		return $this->render('form', ['model' => $model]);
	}
}