<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 14.08.2019
 * Time: 10:22
 */

namespace app\controllers;


use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Users;
use app\models\Profile;

class UserController extends Controller
{
	public function actionUpdate($id)
	{
		$user = Users::findOne($id);
		$profile = Profile::findOne($id);

		if (!isset($user, $profile)) {
			throw new NotFoundHttpException("The user was not found.");
		}

		/*
		$user->scenario = 'update';
		$profile->scenario = 'update';
		*/

		if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {

			$isValid = $user->validate();
			$isValid = $profile->validate() && $isValid;
			if ($isValid) {
				$profile->save();
				$profile->save();
				return $this->redirect(['user/update', 'id' => $id]);
			}
		}

		return $this->render('update', [
			'user' => $user,
			'profile' => $profile,
		]);
	}

	public function actionIndex(){
		return $this->render('index');
	}

	public function actionIndex2(){
		return $this->render('index');
	}
}