<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 14.08.2019
 * Time: 9:52
 */

namespace app\controllers;


use Yii;
use yii\base\Model;
use yii\web\Controller;
use app\models\Setting;


class SettingsController extends Controller
{
	public function actionUpdate()
	{
		$settings = Setting::find()->indexBy('id')->all();

		if (Model::loadMultiple($settings, Yii::$app->request->post()) && Model::validateMultiple($settings)) {
			foreach ($settings as $setting) {
				$setting->save(false);
			}
			return $this->redirect('index');
		}

		return $this->render('update', ['settings' => $settings]);
	}

	public function actionCreate()
	{
		$count = count(Yii::$app->request->post('Setting', []));
		$settings = [new Setting()];
		for($i = 1; $i < $count; $i++) {
			$settings[] = new Setting();
		}
	}
}