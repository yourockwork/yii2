<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 13.08.2019
 * Time: 16:09
 */

namespace app\controllers;


use app\models\FilesUploadForm;
use app\models\FileUploadForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class FilesController extends Controller
{
	/**
	 * Загружаем файл
	 *
	 * @return string|void
	 */
	public function actionUploadfile()
	{
		$model = new FileUploadForm();

		if (Yii::$app->request->isPost) {
			$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
			if ($model->uploadfile()) {
				// file is uploaded successfully
				return;
			}
		}

		return $this->render('uploadfile', ['model' => $model]);
	}

	/**
	 * Загружаем файлы
	 *
	 * @return string|void
	 */
	public function actionUploadfiles()
	{
		$model = new FilesUploadForm();

		if (Yii::$app->request->isPost) {
			$model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
			if ($model->uploadfiles()) {
				// file is uploaded successfully
				return;
			}
		}

		return $this->render('uploadfiles', ['model' => $model]);
	}
}