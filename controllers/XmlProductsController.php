<?php
/**
 * Created by PhpStorm.
 * User: youri
 * Date: 27.06.2019
 * Time: 2:21
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\XmlProductsSearch;

class XmlProductsController extends Controller
{
	/**
	 * @return string
	 */
	public function actionIndex(){
		$searchModel = new XmlProductsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
}