<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 15.08.2019
 * Time: 11:02
 */

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\Country;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\widgets\ListView;

class WidgetsController extends \yii\web\Controller
{
	public function actionDetail(){
		$model = Country::findOne(['code' => 'AU']);
		return $this->render('detail', ['model' => $model]);
	}

	public function actionGrid(){
		$dataProvider = new ActiveDataProvider([
			'query' => Country::find(),
			'pagination' => [
				'pageSize' => 5,
			],
		]);

		return $this->render('grid', ['dataProvider' => $dataProvider]);
	}

	public function actionGrids(){
		$userProvider = new ActiveDataProvider([
			'query' => Users::find(),
			'pagination' => [
				'pageSize' => 5,
				'pageParam' => 'user-page'
			],
			'sort' => [
				'sortParam' => 'user-sort'
			]
		]);

		$countryProvider = new ActiveDataProvider([
			'query' => Country::find(),
			'pagination' => [
				'pageSize' => 5,
				'pageParam' => 'country-page'
			],
			'sort' => [
				'sortParam' => 'country-sort'
			]
		]);

		return $this->render('grids', ['countryProvider' => $countryProvider, 'userProvider' => $userProvider]);
	}

	public function actionDelete($id){
		if (Country::deleteAll(['code' => $id])){
			Yii::$app->session->setFlash('success', 'Удаление выполнено');
		}
		/*else{
			Yii::$app->session->setFlash('fail', 'Удаление не выполнено');
		}*/

		return $this->redirect(['grid']);
	}

	public function actionList(){
		$dataProvider = new ActiveDataProvider([
			'query' => Country::find(),
			'pagination' => [
				'pageSize' => 5,
			],
		]);

		$widget = ListView::widget([
			'dataProvider' => $dataProvider,
//			'itemView' => 'list',
		]);

		return $this->render('list', [
			'model' => new Country(),
			'widget' => $widget,
		]);
	}
}