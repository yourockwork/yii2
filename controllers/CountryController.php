<?php

namespace app\controllers;

use Yii;
use app\models\CountrySearch;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\Country;
use yii\data\Pagination;

class CountryController extends Controller
{
	public function actionFilter()
	{
		$searchModel = new CountrySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->get());

		return $this->render('filter', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	public function actionIndex()
	{
		$query = Country::find();
		//$query2 = new Query();
		//$query3 = new Query();
		//$last_name = 'Igor';
		//$population = 205722000;
		//$query2->select(['code', 'name'])->from('country')->where('population=:population', [':population' => $population])->limit(1)->all();
		//$query3->select(['id', 'email'])->from('user')->where('last_name=:last_name', [':last_name' => $last_name])->limit(10)->all();
		//VarDumper::dump($query2, 10, true);die();

		$pagination = new Pagination([
			'defaultPageSize' => 5,
			'totalCount' => $query->count()
		]);

		$countries = $query->orderBy('name')
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		return $this->render('index', [
			'countries' => $countries,
			'pagination' => $pagination,
		]);
	}

	public function actionShow($code)
	{
		$country = Country::find()->where(['code' => $code])->one();

		return $this->render('show', [
			'country' => $country
		]);
	}
}