<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 01.07.2019
 * Time: 11:30
 */

namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;
use app\components\xmldataprovider\XMLDataProvider;

class XmlProductsSearch extends XmlProducts
{
	public $id;
	public $category_name;
	public $price;

	/**
	 * Определяем поля доступные для поиска
	 * @return array
	 */
	public function rules()
	{
		return [
			[['id', 'category_name', 'price'], 'string'],
		];
	}

	public function search($params){
		$productsArray = new XMLDataProvider(['products' => 'products.xml', 'categories' => 'categories.xml']);
		$filterArray = $params['XmlProductsSearch'];

		$result = array_filter(
			$productsArray->getModels(),
			function ($product, $key) use ($filterArray){
				if ($filterArray['category_name'] == '' && $filterArray['price'] == '' && $filterArray['id'] == ''){
					return true;
				}
				else{
					return (
						$product['category_name'] == $filterArray['category_name'])
						|| ($product['price'] == $filterArray['price'])
						|| ($product['id'] == $filterArray['id']
					);
				}
			}
			,ARRAY_FILTER_USE_BOTH
		);

		$provider = new ArrayDataProvider([
			'allModels' => $result,
			'pagination' => [
				'pageSize' => 10,
			],
			'sort' => [
				'attributes' => ['id', 'category_name', 'price'],
			],
		]);

		return $provider;
	}
}