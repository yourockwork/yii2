<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 13.08.2019
 * Time: 15:53
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FileUploadForm extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $imageFile;
	/**
	 * @var array UploadedFiles
	 */
	public $imageFiles = [];

	public function rules()
	{
		return [
			[['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
		];
	}

	public function uploadfile()
	{
		if ($this->validate()) {
			$this->imageFile->saveAs(Yii::getAlias('@app')."/uploads/" . $this->imageFile->baseName . '.' . $this->imageFile->extension);
			return true;
		} else {
			return false;
		}
	}
}