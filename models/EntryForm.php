<?php
/**
 * Created by PhpStorm.
 * User: youri
 * Date: 17.06.2019
 * Time: 16:08
 */

namespace app\models;

use yii\base\Model;

class EntryForm extends Model
{
	public $name;
	public $email;

	public function rules(){
		return [
			//обязательные поля
			[['name', 'email'], 'required'],
			// В поле email должен быть правильный адрес email
			['email', 'email'],
		];
	}
}