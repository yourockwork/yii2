<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 01.07.2019
 * Time: 9:47
 */

namespace app\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\XMLDataProvider;
use yii\helpers\VarDumper;

class XmlProducts extends Model
{
	/**
	 * @return ArrayDataProvider
	 */
	public function getProducts(){
		// источник данных - это xml файл
		$productsObj = new XMLDataProvider(['products' => 'products.xml', 'categories' => 'categories.xml']);

		$arrayProvider = new ArrayDataProvider([
			'allModels' => $productsObj->getModels(),
			'pagination' => [
				'pageSize' => 10,
			],
			'sort' => [
				'attributes' => ['id', 'category_name', 'price'],
			],
		]);

		return $arrayProvider;
	}
}