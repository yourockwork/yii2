<?php
/**
 * Created by PhpStorm.
 * User: youri
 * Date: 17.06.2019
 * Time: 16:45
 */

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class CountrySearch extends Country
{
	/**
	 * @var string
	 */
	public $createdFrom;

	/**
	 * @var string
	 */
	public $createdTo;

	public function rules()
	{
		// только поля определенные в rules() будут доступны для поиска
		return [
			[['code'], 'string'],
			[['name'], 'string'],
			[['population'], 'integer'],
		];
	}

	public function search($params)
	{
		$query = Country::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// загружаем данные формы поиска и производим валидацию
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		// изменяем запрос добавляя в его фильтрацию
		$query->andFilterWhere(['code' => $this->code]);
		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'population', $this->population]);

		return $dataProvider;
	}
}