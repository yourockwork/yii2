<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 13.08.2019
 * Time: 15:53
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FilesUploadForm extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $imageFile;
	/**
	 * @var array UploadedFiles
	 */
	public $imageFiles = [];

	public function rules()
	{
		return [
			[['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 3],
		];
	}

	public function uploadfiles(){
		if ($this->validate()) {
			foreach ($this->imageFiles as $file) {
				$file->saveAs(Yii::getAlias('@app')."/uploads/" . $file->baseName . '.' . $file->extension);
			}
			return true;
		} else {
			return false;
		}
	}
}