<?php
/**
 * Created by PhpStorm.
 * User: youri
 * Date: 17.06.2019
 * Time: 16:45
 */

namespace app\models;


use yii\db\ActiveRecord;

class Country extends ActiveRecord
{
	public static function tableName(){
		return 'country';
	}
}