<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 14.08.2019
 * Time: 10:19
 */

namespace app\models;


use yii\base\Model;
use yii\db\ActiveRecord;

class Users extends ActiveRecord
{
	public function rules()
	{
		return [
			[['username'], 'safe'],
		];
	}

	public static function tableName(){
		return 'users';
	}
}