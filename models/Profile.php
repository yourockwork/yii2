<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 14.08.2019
 * Time: 10:20
 */

namespace app\models;

use yii\base\Model;
use yii\db\ActiveRecord;

class Profile extends ActiveRecord
{
	public function rules()
	{
		return [
			[['website'], 'safe'],
		];
	}

	public static function tableName(){
		return 'profile';
	}
}