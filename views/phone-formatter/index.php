<?php
/**
 * Created by PhpStorm.
 * User: YouRock
 * Date: 27.06.2019
 * Time: 22:41
 */

use app\components\formatters\PhoneFormatter;

$number = '7777777777';
echo "<p>Введён номер: $number</p>";
echo "<p>В международном формате: " . Yii::$app->formatter->asPhoneNumber($number, 'RU') . "</p>";