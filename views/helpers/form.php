<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

echo Html::beginForm(['order/update', 'id' => '7'], 'post', ['enctype' => 'multipart/form-data']);
	echo Html::input('text', 'username', 'text', ['class' => 'form-control']);
	echo Html::radio('agree', true, ['label' => 'Я согласен']);
	echo Html::dropDownList('list', 1, [
		1 => 'val1',
		2 => 'val2'
	]);
	// (name, default value, dataArray[])
	echo Html::checkboxList('roles', [1, 3], [
		1 => 'val1',
		2 => 'val2',
		3 => 'val3',
	]);
echo Html::endForm();