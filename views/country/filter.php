<?php
use yii\grid\GridView;

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		[
			'label' => 'Код',
			'attribute' => 'code',
		],
		[
			'label' => 'Страна',
			'attribute' => 'name',
		],
		[
			'label' => 'Население',
			'attribute' => 'population',
		],
	],
]);
?>