<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

echo "<h1>Страны</h1>";


echo "<ul>";
    foreach ($countries as $country){
        echo "<li>";
            echo Html::encode("{$country->code} ({$country->name})");
            echo $country->population;
        echo "</li>";
    }
echo "</ul>";

echo LinkPager::widget(['pagination' => $pagination]);