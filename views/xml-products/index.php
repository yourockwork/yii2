<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		[
			'label' => 'Номер',
			'attribute' => 'id',
		],
		[
			'label' => 'Производитель',
			'attribute' => 'category_name',
		],
		[
			'label' => 'Цена',
			'attribute' => 'price',
		],
	],
]);