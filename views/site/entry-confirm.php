<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <p>Вы ввели следующую информацию:</p>

        <ul>
            <li><label>Name</label>: <?= Html::encode($model->name) ?></li>
            <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
        </ul>
    </div>
</div>
