<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hi, <?php echo Html::encode($name) ?>!</h1>
        <p><?php echo Html::encode($message) ?></p>
    </div>
</div>

