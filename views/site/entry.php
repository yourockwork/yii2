<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
    echo $form->field($model, 'name')->label('Ваше имя');
    echo $form->field($model, 'email')->label('Ваш email');
    echo "<div class='form-group'>";
        echo Html::submitButton('Send', ['class' => 'btn btn-primary']);
    echo "</div>";
ActiveForm::end();