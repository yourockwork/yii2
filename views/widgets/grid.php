<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 15.08.2019
 * Time: 11:01
 */
use yii\widgets\DetailView;
use yii\grid\GridView;

/*
if( Yii::$app->session->hasFlash('fail') ){
	echo "<div class='alert alert-success alert-dismissible' role='alert'>";
		echo "<button type='button' class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
		echo Yii::$app->session->getFlash('fail');
	echo "</div>";
}
*/

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		// пронумеруем строки
		['class' => 'yii\grid\SerialColumn'],
		// Обычные поля определенные данными содержащимися в $dataProvider.
		// Будут использованы данные из полей модели.
		[
			'attribute' => 'code',
			'label' => 'Код',
			'format' => 'text',
			'filter' => ['US' => 'USA']
		],
		'name' => [
			'header' => 'Название города',
			'content' => function ($model, $key, $index, $column) {
				return $model->name . " - " . $model->code;
			}
		],
		// Более сложный пример.
		[
			'class' => 'yii\grid\DataColumn', // может быть опущено, поскольку является значением по умолчанию
			'label' => 'Население',
			'attribute' => 'population',
			'value' => function ($data) {
				return $data->population; // $data['population'] для массивов, например, при использовании SqlDataProvider.
			},
		],
		// флаг, галочка (чекбокс).
		[
			'class' => 'yii\grid\CheckboxColumn',
			'header' => 'Чекбокс',

			// вы можете настроить дополнительные свойства здесь.
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'buttons' => [
				// прячем кнопку
				'view' => function ($model, $key, $index) { return false;},
				'update' => function ($model, $key, $index) { return false;},
				'delete' => function ($model, $key, $index) { return "<a href='delete?id=".$index."'>Delete</a>";}
			]
			// вы можете настроить дополнительные свойства здесь.
		],
	],
]);