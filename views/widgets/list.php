<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

echo $widget;
?>

<div class="post">
    <h2><?= Html::encode($model->name) ?></h2>

	<?= HtmlPurifier::process($model->code) ?>
</div>