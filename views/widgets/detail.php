<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 15.08.2019
 * Time: 11:01
 */
use yii\widgets\DetailView;

/*
\yii\helpers\VarDumper::dump($model,10,true);
die();
*/

echo DetailView::widget([
	'model' => $model,
	'attributes' => [
		[                                                  // name свойство зависимой модели owner
			'label' => 'Код страны',
			'value' => $model->code,
			'contentOptions' => ['class' => 'bg-red'],     // настройка HTML атрибутов для тега, соответсвующего value
			'captionOptions' => ['tooltip' => 'Tooltip'],  // настройка HTML атрибутов для тега, соответсвующего label
		],
		'name',
		'population:decimal'
	],
]);