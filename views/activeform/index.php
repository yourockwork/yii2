<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo "<h2>Ajax</h2>";

$form = ActiveForm::begin(['id' => 'country-form', 'action' => '/web/activeform/form','options' => ['class' => 'form-horizontal']]);
	echo $form->field($model, 'code')->textInput()->hint('Введите code')->label('code');
	echo $form->field($model, 'name')->textInput()->hint('введите name')->label('name');
	echo '<div class="form-group">';
		echo Html::submitButton('Send	', ['class' => 'btn btn-primary']);
	echo '</div>';
ActiveForm::end();

echo "<h2>Ajax</h2>";

$form = ActiveForm::begin(['id' => 'country-ajax-form', 'action' => '/web/activeform/ajax','options' => ['class' => 'form-horizontal']]);
echo $form->field($model, 'code')->textInput()->hint('Введите code')->label('code');
echo $form->field($model, 'name')->textInput()->hint('введите name')->label('name');
echo '<div class="form-group">';
echo Html::submitButton('Send	', ['class' => 'btn btn-primary']);
echo '</div>';
ActiveForm::end();

echo "<button class='js-plus'>Add population field</button>";
