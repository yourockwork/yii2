<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php
    // в видах есть события, на которые можно повесить обработчик
    Yii::$app->view->on(View::EVENT_END_BODY, function () {
	    echo date('Y-m-d');
    });
?>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Countries', 'url' => ['/country/filter']],
            ['label' => 'XmlProducts', 'url' => ['/xml-products']],
            ['label' => 'PhoneFormatter', 'url' => ['/phone-formatter']],
			['label' => 'Widgets', 'url' => ['#'], 'items' =>
				[
					['label' => 'DetailView', 'url' => ['/widgets/detail']],
					['label' => 'ListView', 'url' => ['/widgets/list']],
					['label' => 'GridView', 'url' => ['/widgets/grid']],
					['label' => 'GridViews', 'url' => ['/widgets/grids']],
				],
			],
            ['label' => 'Files', 'url' => ['#'], 'items' =>
                [
                    ['label' => 'Upload file', 'url' => ['/files/uploadfile']],
                    ['label' => 'Upload files', 'url' => ['/files/uploadfiles']],
                ],
            ],
			['label' => 'Helpers', 'url' => ['#'], 'items' =>
				[
					['label' => 'Form', 'url' => ['/helpers/form']],
				],
			],
			['label' => 'ActiveForm', 'url' => ['#'], 'items' =>
				[
					['label' => 'ActiveForm', 'url' => ['/activeform/index']],
				],
			],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php
		// передаем параметры между видами
		// $this->params['myparam'] = 'my text';
        ?>

        <!-- задаем блок -->
        <?php /*if (isset($this->blocks['block1'])): */?><!--
			<?/*= $this->blocks['block1'] */?>
		<?php /*else: */?>
            <div class="blockquote">
                default content ...
            </div>
		--><?php /*endif; */?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
