<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 14.08.2019
 * Time: 10:28
 */
$form = ActiveForm::begin([
	'id' => 'user-update-form',
	'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($user, 'username') ?>

	...other input fields...

<?= $form->field($profile, 'website') ?>

<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>