<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
	'id' => 'user-update-form',
	'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($user, 'username') ?>
<?= $form->field($profile, 'website') ?>

<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>