<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 18.09.2019
 * Time: 12:20
 */

namespace app\modules\test\controllers;

use yii\web\Controller;

class TestController extends Controller
{
	public function actionIndex()
	{
		return $this->render('index');
	}
}