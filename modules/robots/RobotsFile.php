<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 20.09.2019
 * Time: 16:10
 */

namespace app\modules\robots;

use Yii;

class RobotsFile
{
	protected $objRoutes;

	public function createRobotsFile()
	{
		$routes = new RoutesGenerator();
		$arrRoutes = $routes->generateArrRoutes();
		$robots = fopen(\Yii::getAlias('@app')."\\robots.txt", 'w') or die("не удалось создать файл");

		fwrite($robots, "User-agent: * \r\n");

		foreach ($arrRoutes as $item){
			fwrite($robots, "Allow: /$item \r\n");
		}

		fclose($robots);
	}
}