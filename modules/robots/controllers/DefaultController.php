<?php

namespace app\modules\robots\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Default controller for the `Robots` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest(){
		return $this->render('index');
	}

	public function actionYourock(){
		return $this->render('index');
	}

	public function actions(){
		return [
			'hi' => [
				'class' => 'app\components\HelloWorldAction',
			],
		];
	}
}