<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 19.09.2019
 * Time: 10:33
 */

namespace app\modules\robots;


use app\modules\robots\controllers\DefaultController;
use app\modules\robots\controllers\TestController;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class RoutesGenerator
{

	protected function generateArrURI(){
		$res = [];

		foreach ($this->allModules() as $module){
			foreach ($this->getControllers($module) as $controller){
				$res[$module][$controller] = $this->getActions($module, $controller);
			}
		}

		return $res;
	}

	/**
	 * Список модулей
	 *
	 * @return array
	 */
	protected function allModules()
	{

		foreach (\Yii::$app->modules as $module => $par){
			$res[] = $module;
		}

		$needle = ['gii', 'debug'];

		// не учитываем стандартные модули
		foreach ($needle as $item){
			if(($key = array_search($item, $res)) !== FALSE){
				unset($res[$key]);
			}
		}

//		VarDumper::dump($res, 10, true);die();

		return array_filter($res, function ($var) {
			return !is_null($var);
		});
	}

	protected function getControllers($module)
	{
		$res = [];
		$files = FileHelper::findFiles(\Yii::getAlias('@modules') . "\\" . $module . "", [
			'only' => [
				'*Controller.php'
			]
		]);

		if (isset($files[0])) {
			foreach ($files as $index => $file) {
				preg_match('/\w+.php/', $file, $matches);
				$res[] = substr($matches[0], 0, strrpos($matches[0], '.'));
			}
		}

		return preg_replace('/controller/', '', array_map('strtolower', $res));
	}

	protected function getActions($module, $controller)
	{
		return ArrayHelper::merge(
			// получаем массив action-методов, отсекая префикс action и в нижнем регистре
			array_map('strtolower',
				preg_replace('/^action/', '',
					array_filter(get_class_methods('app\modules\\' . $module . '\controllers\\' . ucfirst($controller) . 'Controller'),
						function ($val) {
							return preg_match('/^action[A-Z]\w+/', $val);
						})
				)
			),
			// actions могут задаваться классами, нужно как-то учесть и это
			[]
		);
	}

	public function generateArrRoutes()
	{
		$arrRoutes = [];

		foreach ($this->generateArrURI() as $module => $controllers) {
			$arrRoutes[] = $module;
			foreach ($controllers as $controller => $actions) {
				$arrRoutes[] = $module . "/" . $controller;
				foreach ($actions as $key => $action) {
					$arrRoutes[] = $module . "/" . $controller . "/" . $action;
				}
			}
		}

		return $arrRoutes;
	}
}