<?php

namespace app\modules\robots;

use yii\helpers\VarDumper;

/**
 * Robots module definition class
 */
class Robots extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\robots\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $robots = new RobotsFile();
		$robots->createRobotsFile();
    }
}