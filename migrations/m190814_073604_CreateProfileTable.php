<?php

use yii\db\Migration;

/**
 * Class m190814_073604_CreateProfileTable
 */
class m190814_073604_CreateProfileTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%profile}}', [
			'id' => $this->primaryKey(),
			'text' => $this->string()->notNull()->unique(),
			'user_id' => $this->integer()->notNull(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%profile}}');
    }
}