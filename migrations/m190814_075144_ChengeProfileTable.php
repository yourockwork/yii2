<?php

use yii\db\Migration;

/**
 * Class m190814_075144_ChengeProfileTable
 */
class m190814_075144_ChengeProfileTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('profile', 'website', $this->string(64));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn('profile', 'website');
    }
}
