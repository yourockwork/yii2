$('#country-form').on('beforeSubmit', function (e) {
    if (!confirm("Everything is correct. Submit?")) {
        return false;
    }
    return true;
});

var $form = $('#country-ajax-form');
$form.on('beforeSubmit', function() {
    var data = $form.serialize();
    $.ajax({
        url: $form.attr('action'),
        type: 'POST',
        data: data,
        success: function (data) {
            console.log(data);
        },
        error: function(jqXHR, errMsg) {
            alert(errMsg);
        }
    });
    return false; // prevent default submit
});

/*
$('.js-plus').on('click', function () {
    $('#country-ajax-form').yiiActiveForm('add', {
        id: 'population',
        name: 'population',
        container: '.field-population',
        input: '#population',
        error: '.help-block',
    });
});*/
