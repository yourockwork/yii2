<?php
/**
 * Created by PhpStorm.
 * User: Alekseev_UA
 * Date: 16.08.2019
 * Time: 9:08
 */

namespace app\components\urlmap;

use Yii;
use app\components\urlmap\CustomIteratorInterface;

class Urlmap
{
	protected $objIterator;

	public function __construct(CustomIteratorInterface $objIterator)
	{
		$this->objIterator = $objIterator;
	}

	public function redirect($currentUrl){
		// обходим objIterator
		foreach ($this->objIterator as $key => $row) {
			// регистронезависимо сравниваем uri
			if (strcasecmp(trim($row[0]), rtrim($currentUrl, "/")) === 0){
				$newUrl = $row[1]; // тут хранится новый uri
				$status = $row[2]; // тут хранится статус
				break;
			}
		}

		if ($newUrl && (strlen($newUrl) > 0)){
			Yii::$app->response->redirect($newUrl, $status)->send();
		}
	}
}