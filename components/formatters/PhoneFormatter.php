<?php
/**
 * Created by PhpStorm.
 * User: YouRock
 * Date: 27.06.2019
 * Time: 20:38
 */

namespace app\components\formatters;


class PhoneFormatter extends \yii\i18n\Formatter
{
	public function asPhoneNumber($number, $code = 'RU'){
		if ($number == null){
			return $this->nullDisplay;
		}
		else{
			return $this->phoneFormatter($number, $code);
		}
	}


	public function phoneFormatter($number, $code){
		// удаляем все нецифровые символы
		$number = preg_replace('/\D/','', $number);

		if (strlen($number) == 6){
			$number = preg_replace('/(\d{3})(\d{3})/','$1-$2', $number);
		}
		elseif (strlen($number) == 7){
			$number = preg_replace('/(\d{3})(\d{4})/','$1-$2', $number);
		}
		elseif (strlen($number) == 10){
			$number = preg_replace('/(\d{3})(\d{3})(\d{2})(\d{2})/','($1) $2-$3-$4', $number);
		}
		elseif (strlen($number) == 11){
			$number = preg_replace('/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/','$1 ($2) $3-$4-$5', $number);
		}
		else{
			return 'Номер не соответствует ни одному из известных форматов';
		}

		$number = $this->getCountryCode($code) . ' ' . $number;

		return $number;
	}

	public function getCountryCode($code = 'RU'){
		if ($code == 'RU') {
			return '+7';
		}
		elseif ($code == 'UA'){
			return '+380';
		}
		elseif ($code == 'US'){
			return '+1';
		}

		return null;
	}
}