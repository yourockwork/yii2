<?php
/**
 * Created by PhpStorm.
 * User: youri
 * Date: 19.06.2019
 * Time: 17:43
 */

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Тестовый виджет
 * Class HelloWidget
 * @package app\components
 */
class HelloWidget extends Widget
{
	public $message;
	public $name;

	/*public function init()
	{
		parent::init();
		ob_start();
	}

	public function run()
	{

		$content = ob_get_clean();
		$result = "<p>$content</p>";
		return Html::encode($result);
	}*/

	public function init(){
		parent::init();

		if ($this->message === null) {
			$this->message = 'Hello World';
		}
		if ($this->name === null) {
			$this->name = 'username';
		}
	}

	public function run(){
		// подключаем к виджету view
		return $this->render('hello', ['message' => Html::encode($this->message), 'name' => Html::encode($this->name)]);
//		return Html::encode($this->message).", ".Html::encode($this->name);
	}
}