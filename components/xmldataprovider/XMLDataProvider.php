<?php

namespace app\components\xmldataprovider;

use yii\data\BaseDataProvider;
use yii\helpers\VarDumper;

class XMLDataProvider extends BaseDataProvider
{
	/**
	 * @var string имя xml-файлов для чтения
	 */
	public $products;
	public $categories;

	/**
	 * @var string|callable имя столбца с ключом или callback-функция, возвращающие его
	 */
	public $key;

	/**
	 * Обьекты xml файлов, полученных при помощи SimpleXMLElement
	 * @var SplFileObject
	 */
	protected $productsObject;
	protected $categoriesObject;

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->productsObject = new \SimpleXMLElement(file_get_contents(\Yii::getAlias("@xml") . $this->products));
		$this->categoriesObject = new \SimpleXMLElement(file_get_contents(\Yii::getAlias("@xml") . $this->categories));
	}

	protected function prepareModels()
	{
		$products = [];
		for ($i = 0; $i < $this->productsObject->count(); $i++) {
			$product = $this->productsObject->children()[$i];

			for ($j = 0; $j < $this->categoriesObject->count(); $j++) {
				$category = $this->categoriesObject->children()[$j];

				// вытаскиваем имя категории из связанного xml
				if ((int)$category->id == (int)$product->categoryId) {
					$product->category_name = $category->name;
				}
			}
			// переводим в массив, тем самым подготавливаем данные для ArrayDataProvider
			$products[] = (array)$product;
		}

		return $products;
	}

	protected function prepareKeys($models)
	{
		if ($this->key !== null) {
			$keys = [];

			foreach ($models as $model) {
				if (is_string($this->key)) {
					$keys[] = $model[$this->key];
				} else {
					$keys[] = call_user_func($this->key, $model);
				}
			}

			return $keys;
		} else {
			return array_keys($models);
		}
	}

	protected function prepareTotalCount()
	{
		return $this->productsObject->count();
	}
}