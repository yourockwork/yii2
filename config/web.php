<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
		'@xml' => '@app/xml/',
		'@components' => '@app/components'
    ],
	'on beforeAction' => function ($event) {
		$container = new \yii\di\Container();
		$container->set('app\components\urlmap\CustomIteratorInterface',
			['class' => 'app\components\urlmap\CsvIterator'],
			// путь к csv файлу
			[$file = Yii::getAlias('@components')."/urlmap/redirect.csv"]
		);
		$container->set('Urlmap', 'app\components\urlmap\Urlmap');
		$urlMap = $container->get('Urlmap');
		$urlMap->redirect(Yii::$app->request->url);

		/*
		$filePath = Yii::getAlias('@components')."/urlmap/redirect.csv";
		$urlMap = new \app\components\urlmap\Urlmap(
			new \app\components\urlmap\CsvIterator($filePath)
		);
		$urlMap->redirect(Yii::$app->request->url);
		*/

	},

	//указывает действие контроллера, которое должно обрабатывать все входящие запросы от пользователя
	/*
	'catchAll' => [
		'offline/notice',
		'param1' => 'value1',
		'param2' => 'value2',
	],
	*/

	'modules' => [

	],
/*
	'container' => [

		'definitions' => [
			'app\components\urlmap' => [
				'urlmap' => ''
			]
		],
		'sigletones' => [

		],
	],
*/
    'components' => [
		'assetManager' => [
			'linkAssets' => true,
			'appendTimestamp' => true

			/*
			// отключаем ресурсы yii2
			'bundles' => [
				'yii\bootstrap\BootstrapAsset' => false,
				'yii\jui\JuiAsset' => false,
				'yii\bootstrap\BootstrapPluginAsset' => false,
				'yii\web\JqueryAsset' => false,
				'yii\web\YiiAsset' => false,
			]
			*/

			// 'bundles' => false // отключить все js, css скрипты yii2
		],
		'formatter' => [
			'class' => 'app\components\formatters\PhoneFormatter',
		],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'h_qzjFe7IUok9OwxVEUx5UdPBkcWd6rn',
			//'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'country/show/<code:\w{2}>' => 'country/show',
			/*
				'about' => 'site/about',
				'news' => 'test/index',
				'news/<id:\d+>' => 'test/view',
				'mail' => 'mail/send',
				'employee' => 'employee/index',
				'newsletter' => 'newsletter/index',
				'alias' => 'alias/index',
				'gallery' => 'gallery/index',
			*/
			],
		],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
